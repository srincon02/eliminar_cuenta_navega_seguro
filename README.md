# Navega Seguro

## Eliminación de la cuenta

Si deseas eliminar tu cuenta de nuestra aplicación, sigue estos pasos:

1. Abre la aplicación e inicia sesión con tu cuenta.
2. Ve a la sección de configuración de tu perfil.
3. Busca la opción "Eliminar cuenta".
4. Confirma que deseas eliminar tu cuenta ingresando la palabra "Eliminar" en el recuadro.

Por favor, ten en cuenta que al eliminar tu cuenta, todos los datos asociados a tu cuenta también serán eliminados y no podrán ser recuperados.

Si tienes algún problema o necesitas ayuda adicional, no dudes en contactarnos a través de nuestro soporte al correo electrónico: javelandia@ucatolica.edu.co
